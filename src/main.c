/* main.c - initialize think & read commandline args
   Copyright (C) 1999, 2000 Peter Teichman

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, 
   USA.  */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
#include <glade/glade.h>

#include "support.h"
#include "think.h"

int 
main (int argc, char *argv[])
{
	GtkWidget *outline;
	gchar *filename = NULL;
	char **args;
	poptContext ctx;

#ifdef ENABLE_NLS
	bindtextdomain (PACKAGE, PACKAGE_LOCALE_DIR);
	textdomain (PACKAGE);
#endif

	glade_gnome_init ();
	gnome_init_with_popt_table ("think", VERSION, argc, argv,
				    NULL, 0, &ctx);
	args = (char **)poptGetArgs(ctx);

	if (args) 
		filename = args[0];

	poptFreeContext (ctx);

	outline = think_window_new (filename);
	gtk_widget_show (outline);

	gtk_main();
	return 0;
}
