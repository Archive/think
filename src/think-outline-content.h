/* think-outline-content.h: base content item for a ThinkOutline
   Copyright (C) 1999, 2000 Peter Teichman

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, 
   USA.  */

#ifndef __THINK_OUTLINE_CONTENT_H__
#define __THINK_OUTLINE_CONTENT_H__

typedef struct _ThinkOutlineContent ThinkOutlineContent;
typedef struct _ThinkOutlineContentClass ThinkOutlineContentClass;
typedef struct _ThinkOutlineContentPrivate ThinkOutlineContentPrivate;

typedef GnomeCanvasItem * (*ThinkOutlineContentDrawFunc) (GnomeCanvasGroup *group, ThinkOutlineContent *content);

#include "think-outline.h"

#include <libgnome/gnome-defs.h>
#include <libgnomeui/gnome-canvas.h>

BEGIN_GNOME_DECLS

#define THINK_OUTLINE_CONTENT(obj)           (GTK_CHECK_CAST ((obj), think_outline_content_get_type (), ThinkOutlineContent))
#define THINK_OUTLINE_CONTENT_CLASS(klass)   (GTK_CHECK_CLASS_CAST ((klass), THINK_TYPE_OUTLINE_CONTENT, ThinkOutlineContentClass))
#define THINK_IS_OUTLINE_CONTENT(content)    (GTK_CHECK_TYPE ((content), think_outline_content_get_type ()))

/* class ThinkOutlineContent begins. This is an abstract base class. Any
   item that can be stored in an outline must derive from this. */

struct _ThinkOutlineContent {
	GtkObject parent;

	GNode *gnode;
	GnomeCanvasItem *item;

	ThinkOutlineContentPrivate *priv;
};

struct _ThinkOutlineContentClass {
	GnomeCanvasGroupClass parent_class;

	void (* expand)          (ThinkOutlineContent *content);
	void (* collapse)        (ThinkOutlineContent *content);
	void (* expanded_toggle) (ThinkOutlineContent *content);

	void (* drag_enter)       (ThinkOutlineContent *content,
				   ThinkOutlineContent *dragged);
	void (* drag_leave)       (ThinkOutlineContent *content,
				   ThinkOutlineContent *dragged);
	void (* item_drop)        (ThinkOutlineContent *content,
				   ThinkOutlineContent *dropped);
	void (* item_drop_after)  (ThinkOutlineContent *content,
				   ThinkOutlineContent *dropped);
	void (* item_drop_before) (ThinkOutlineContent *content,
				   ThinkOutlineContent *dropped);
};

GtkType    think_outline_content_get_type (void);

GNode *    think_outline_content_new (void);
GNode *    think_outline_content_new_with_data (gpointer data,
						ThinkOutlineContentDrawFunc func);
void       think_outline_content_destroy (GNode *gnode);

gpointer   think_outline_content_get_data (ThinkOutlineContent *content);
void       think_outline_content_set_data (ThinkOutlineContent *content,
					   gpointer data);

void    think_outline_content_set_draw_func (ThinkOutlineContent *content,
					     ThinkOutlineContentDrawFunc func);

guint      think_outline_content_expanded (ThinkOutlineContent *content);

void       think_outline_content_expand (ThinkOutlineContent *content);
void       think_outline_content_collapse (ThinkOutlineContent *content);

void       think_outline_content_set_dirty (ThinkOutlineContent *content);

/* for use by ThinkOutline */
void       think_outline_content_show (ThinkOutline *outline,
				       ThinkOutlineContent *content,
				       double x, double y, 
				       double *width, double *height);
void       think_outline_content_hide (ThinkOutlineContent *content);

END_GNOME_DECLS

#endif /* __THINK_OUTLINE_CONTENT_H__ defined */
