/* think-outline.c - Think's outline widget
   Copyright (C) 1999, 2000 Peter Teichman

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.  */

#include <config.h>
#include <gnome.h>
#include "think-outline.h"

struct _ThinkOutlinePrivate
{
	ThinkOutlineContent *selection;
	GnomeCanvasItem *select_box;
};

enum {
	OUTLINE_REDRAW,
	SELECT,
	ITEM_MOTION,
	ITEM_RELEASE,
	SELECT_NEXT,
	SELECT_PREVIOUS,
	SELECT_CHILD,
	SELECT_PARENT,
	TOGGLE_EXPANDED,
	LAST_OUTLINE_SIGNAL
};

/* outline functions */
static void think_outline_class_init    (ThinkOutlineClass *class);
static void think_outline_init          (ThinkOutline *outline);

static void think_outline_redraw_real   (ThinkOutline *outline);
static void think_outline_select_real   (ThinkOutline *outline,
					 ThinkOutlineContent *content);
static void think_outline_item_motion_real (ThinkOutline *outline,
					    gdouble x, gdouble y);
static void think_outline_item_release_real (ThinkOutline *outline,
					     gdouble x, gdouble y);
static void think_marshal_NONE__DOUBLE_DOUBLE (GtkObject     *object,
					       GtkSignalFunc func,
					       gpointer      func_data,
					       GtkArg        *args);

static void think_outline_select_next_real     (ThinkOutline *outline);
static void think_outline_select_previous_real (ThinkOutline *outline);
static void think_outline_select_child_real    (ThinkOutline *outline);
static void think_outline_select_parent_real   (ThinkOutline *outline);
static void think_outline_toggle_expanded_real (ThinkOutline *outline);

static GnomeCanvasClass *outline_parent_class;
static guint outline_signals[LAST_OUTLINE_SIGNAL] = { 0 };

double THINK_OUTLINE_Y_OFFSET=3.0;
static double X_OFFSET=10.0;
static double Y_INITIAL=1.0;

/* make our own marshal, for signal handling */
typedef void (*GtkSignal_NONE__DOUBLE_DOUBLE) (GtkObject *object,
					       double arg1,
					       double arg2,
					       gpointer user_data);
void think_marshal_NONE__DOUBLE_DOUBLE (GtkObject     *object,
					GtkSignalFunc func,
					gpointer      func_data,
					GtkArg        *args)
{
	GtkSignal_NONE__DOUBLE_DOUBLE rfunc;
	rfunc = (GtkSignal_NONE__DOUBLE_DOUBLE) func;
	(* rfunc) (object,
		   GTK_VALUE_DOUBLE (args[0]),
		   GTK_VALUE_DOUBLE (args[1]),
		   func_data);
}


GtkType
think_outline_get_type (void)
{
	static GtkType think_outline_type = 0;

	if(!think_outline_type) {
		GtkTypeInfo think_outline_info = {
			"ThinkOutline",
			sizeof (ThinkOutline),
			sizeof (ThinkOutlineClass),
			(GtkClassInitFunc) think_outline_class_init,
			(GtkObjectInitFunc) think_outline_init,
			NULL,
			NULL,
			(GtkClassInitFunc) NULL
		};

		think_outline_type = gtk_type_unique (gnome_canvas_get_type(), 
						      &think_outline_info);
	}

	return think_outline_type;
}

static void
think_outline_class_init (ThinkOutlineClass *klass)
{
	GtkObjectClass *object_class;
	GtkWidgetClass *widget_class;
  
	object_class = (GtkObjectClass *) klass;
	widget_class = (GtkWidgetClass *) klass;
  
	outline_parent_class = gtk_type_class (gnome_canvas_get_type ());

	outline_signals[OUTLINE_REDRAW] =
		gtk_signal_new ("outline_redraw",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ThinkOutlineClass, outline_redraw),
				gtk_marshal_NONE__NONE,
				GTK_TYPE_NONE, 0);

	outline_signals[SELECT] =
		gtk_signal_new ("select",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ThinkOutlineClass, select),
				gtk_marshal_NONE__POINTER,
				GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

	outline_signals[ITEM_MOTION] =
		gtk_signal_new ("item_motion",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ThinkOutlineClass, item_motion),
				think_marshal_NONE__DOUBLE_DOUBLE,
				GTK_TYPE_NONE, 2, GTK_TYPE_DOUBLE, GTK_TYPE_DOUBLE);

	outline_signals[ITEM_RELEASE] =
		gtk_signal_new ("item_release",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ThinkOutlineClass, item_release),
				think_marshal_NONE__DOUBLE_DOUBLE,
				GTK_TYPE_NONE, 2, GTK_TYPE_DOUBLE, GTK_TYPE_DOUBLE);

	outline_signals[SELECT_NEXT] =
		gtk_signal_new ("select_next",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ThinkOutlineClass, select_next),
				gtk_marshal_NONE__NONE,
				GTK_TYPE_NONE, 0);

	outline_signals[SELECT_PREVIOUS] =
		gtk_signal_new ("select_previous",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ThinkOutlineClass, select_previous),
				gtk_marshal_NONE__NONE,
				GTK_TYPE_NONE, 0);

	outline_signals[SELECT_CHILD] =
		gtk_signal_new ("select_child",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ThinkOutlineClass, select_child),
				gtk_marshal_NONE__NONE,
				GTK_TYPE_NONE, 0);

	outline_signals[SELECT_PARENT] =
		gtk_signal_new ("select_parent",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ThinkOutlineClass, select_parent),
				gtk_marshal_NONE__NONE,
				GTK_TYPE_NONE, 0);

	outline_signals[TOGGLE_EXPANDED] =
		gtk_signal_new ("toggle_expanded",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ThinkOutlineClass, toggle_expanded),
				gtk_marshal_NONE__NONE,
				GTK_TYPE_NONE, 0);

	gtk_object_class_add_signals (object_class, outline_signals, 
				      LAST_OUTLINE_SIGNAL);

	klass->outline_redraw = think_outline_redraw_real;
	klass->select = think_outline_select_real;
	klass->item_motion = think_outline_item_motion_real;
	klass->item_release = think_outline_item_release_real;

	klass->select_next = think_outline_select_next_real;
	klass->select_previous = think_outline_select_previous_real;
	klass->select_child = think_outline_select_child_real;
	klass->select_parent = think_outline_select_parent_real;
	klass->toggle_expanded = think_outline_toggle_expanded_real;
}

static void
think_outline_init (ThinkOutline *outline)
{
	outline->dragged = NULL;
	outline->last_entered = NULL;
	outline->drag_indicator = NULL;

	outline->priv = g_new (ThinkOutlinePrivate, 1);
}

GtkWidget *
think_outline_new (void)
{
	ThinkOutline *outline;

	outline = gtk_type_new (think_outline_get_type ());

	outline->tree = NULL;
	outline->priv->selection = NULL;
	outline->priv->select_box = NULL;

	return GTK_WIDGET (outline);
}

GtkWidget *
think_outline_new_with_tree (GNode *tree)
{
	ThinkOutline *outline = THINK_OUTLINE (think_outline_new ());
	think_outline_set_tree (outline, tree);

	return GTK_WIDGET (outline);
}

void
think_outline_set_tree (ThinkOutline *outline, GNode *tree)
{
	g_return_if_fail (outline != NULL);
	g_return_if_fail (tree != NULL);

	outline->tree = tree;
}

void
think_outline_redraw (ThinkOutline *outline)
{
	g_return_if_fail (outline != NULL);
	g_return_if_fail (outline->tree != NULL);

	think_outline_redraw_real (outline);
}

static void
think_outline_redraw_real (ThinkOutline *outline)
{
	GNode *gnode, *last_expanded;
	gint depth;
	double item_width=0., max_width=0., height=0., item_height=0.;

	g_return_if_fail (outline != NULL);
	g_return_if_fail (outline->tree != NULL);

	gnode = g_node_first_child (outline->tree);

	last_expanded = NULL;
	depth = 0;
	height = Y_INITIAL;

	if (outline->priv->select_box) {
		gtk_object_destroy (GTK_OBJECT (outline->priv->select_box));
		outline->priv->select_box = NULL;
	}

	while (gnode && (depth >= 0)) /* once the depth goes below
					 zero, we're above the subtree
					 we want to deal with */
	{
		if (last_expanded)
			think_outline_content_hide 
			  (THINK_OUTLINE_CONTENT (gnode->data));
		else {
			think_outline_content_show (outline,
						    THINK_OUTLINE_CONTENT 
						    (gnode->data),
						    (X_OFFSET * depth), 
						    height, &item_width,
						    &item_height);
			height += item_height + THINK_OUTLINE_Y_OFFSET;

			if (item_width > max_width)
				max_width = item_width;
		}

		/* stuff from here on down is just to traverse the
		   tree in order */
		if (g_node_first_child (gnode)){
			if (!think_outline_content_expanded 
			    (THINK_OUTLINE_CONTENT (gnode->data)) 
			    && !last_expanded)
				last_expanded = gnode;

			depth++;
			gnode = g_node_first_child (gnode);
		} else {
			while (!g_node_next_sibling (gnode)) {
				if (gnode == NULL)
					break;

				depth--;
				gnode = gnode->parent;
	      
				if (gnode == last_expanded)
					last_expanded = NULL;

				if (G_NODE_IS_ROOT (gnode))
					gnode = NULL;
			}

			if (gnode)
				gnode = g_node_next_sibling (gnode);
		}
	}

	/* 10000.0 chosen arbitrarily. Any number bigger than the
	   possible window size will work. This will render a bit
	   incorrectly if the user has a larger outline widget than
	   10000x10000 in either direction. */
	gnome_canvas_set_scroll_region (GNOME_CANVAS (outline), 0., 0., 
		max_width+10000.0, height+10000.0);
	gtk_widget_set_usize (GTK_WIDGET (outline), max_width, height);
}

void
think_outline_select (ThinkOutline *outline, ThinkOutlineContent *content)
{
	g_return_if_fail (outline != NULL);

	if (outline->priv->selection == content){
		think_outline_draw_box (outline, content);
	} else {
		if (outline->priv->selection) {
			outline->priv->selection = NULL;
			gtk_signal_emit 
				(GTK_OBJECT (outline), gtk_signal_lookup 
				 ("select", GTK_OBJECT_TYPE (outline)),
				 NULL);
		}
		outline->priv->selection = content;
		gtk_signal_emit (GTK_OBJECT (outline), 
				 gtk_signal_lookup ("select", 
						    GTK_OBJECT_TYPE (outline)),
				 content);
	}
}

void
think_outline_select_toggle (ThinkOutline *outline, 
			     ThinkOutlineContent *content)
{
	g_return_if_fail (outline != NULL);

	if (outline->priv->selection == content)
		think_outline_unselect (outline);
	else
		think_outline_select (outline, content);
}

ThinkOutlineContent *
think_outline_selection (ThinkOutline *outline)
{
	g_return_val_if_fail (outline != NULL, NULL);

	return outline->priv->selection;
}

void
think_outline_unselect (ThinkOutline *outline)
{
	g_return_if_fail (outline != NULL);

	outline->priv->selection = NULL;

	gtk_signal_emit (GTK_OBJECT (outline), 
			 gtk_signal_lookup ("select", 
					    GTK_OBJECT_TYPE (outline)),
			 outline->priv->selection);
}

void think_outline_draw_box (ThinkOutline *outline, 
			     ThinkOutlineContent *content)
{
	if (outline->priv->select_box) {
		gtk_object_destroy (GTK_OBJECT (outline->priv->select_box));
		outline->priv->select_box = NULL;
	}

	if (content) {
		double x1, x2, y1, y2;
      
		gnome_canvas_item_get_bounds (GNOME_CANVAS_ITEM 
					      (content->item),
					      &x1, &y1, &x2, &y2);

		outline->priv->select_box = 
			gnome_canvas_item_new (GNOME_CANVAS_GROUP
					       (GNOME_CANVAS (outline)->root),
					       gnome_canvas_rect_get_type (),
					       "x1", (double)x1-1.0, 
					       "y1", (double)y1-1.0,
					       "x2", (double)x2, 
					       "y2", (double)y2,
					       "outline_color", "black",
					       "width_pixels", 1, NULL);
	}
}

static void
think_outline_select_real (ThinkOutline *outline,
			   ThinkOutlineContent *content)
{
	think_outline_draw_box (outline, content);
}

static void
think_outline_item_motion_real (ThinkOutline *outline, gdouble x, gdouble y)
{
}

static void
think_outline_item_release_real (ThinkOutline *outline, gdouble x, gdouble y)
{
}

void 
think_outline_select_next_real (ThinkOutline *outline)
{
	ThinkOutlineContent *content = think_outline_selection (outline);

	if (content == NULL)
		return;

	if (content->gnode->next) {
		think_outline_select 
			(outline, 
			 THINK_OUTLINE_CONTENT (content->gnode->next->data));
	}
}

void 
think_outline_select_previous_real (ThinkOutline *outline)
{
	ThinkOutlineContent *content = think_outline_selection (outline);

	if (content == NULL)
		return;

	if (content->gnode->prev) {
		think_outline_select 
			(outline, 
			 THINK_OUTLINE_CONTENT (content->gnode->prev->data));
	}
}

void 
think_outline_select_child_real (ThinkOutline *outline)
{ 
	ThinkOutlineContent *content = think_outline_selection (outline);

	if (content == NULL)
		return;

	if (content->gnode->children) {
		think_outline_select 
			(outline, 
			 THINK_OUTLINE_CONTENT (content->gnode->children->data));
	}
}

void 
think_outline_select_parent_real (ThinkOutline *outline)
{ 
	ThinkOutlineContent *content = think_outline_selection (outline);

	if (content == NULL)
		return;

	if (content->gnode->parent && 
	    (content->gnode->parent != outline->tree)) {
		think_outline_select 
			(outline, 
			 THINK_OUTLINE_CONTENT (content->gnode->parent->data));
	}
}

void 
think_outline_toggle_expanded_real (ThinkOutline *outline)
{
	ThinkOutlineContent *content = think_outline_selection (outline);

	if (content == NULL)
		return;

	if (think_outline_content_expanded (content))
		think_outline_content_collapse (content);
	else
		think_outline_content_expand (content);

	think_outline_content_set_dirty (content);
	think_outline_redraw (outline);
}
