/* outline.c - populate a ctree from an emacs outline-mode file
   Copyright (C) 1999, 2000 Peter Teichman

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.  */

#include <config.h>
#include <stdio.h>
#include <gnome.h>
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>
#include "outline.h"
#include "think.h"
#include "think-outline.h"

static void
load_node_xml (GNode **gnode, xmlNodePtr node)
{
	ThinkNode *tnode;
	GNode *new_item;
	ThinkOutlineContent *content;

	new_item = new_think_gnode ();
	content = THINK_OUTLINE_CONTENT (new_item->data);
	tnode = (ThinkNode *)think_outline_content_get_data (content);

	g_free (tnode->title);
	tnode->title = xmlGetProp (node, "title");
	tnode->text = xmlGetProp (node, "text");

	if (xmlGetProp (node, "expanded")
	    && !g_strncasecmp (xmlGetProp (node, "expanded"), "no", 2)) {
		think_outline_content_collapse (content);
	}

	g_node_append (*gnode, new_item);

	if (node->childs)
		load_node_xml (&new_item, node->childs);

	if (node->next)
		load_node_xml (gnode, node->next);
}

gpointer
outline_load_xml (gchar *filename, GNode **root)
{
	xmlDocPtr doc;
	xmlNodePtr node;

	doc = xmlParseFile (filename);

	if (!doc)
		return NULL;

	node = doc->root->childs;

	if (node)
		load_node_xml (root, node);

	xmlFreeDoc (doc);
	return root;
}

static void
save_node_xml (GNode *gnode, xmlNodePtr node)
{
	xmlNodePtr tree;
	ThinkOutlineContent *content;
	ThinkNode *tnode;

	content = THINK_OUTLINE_CONTENT (gnode->data);
	tnode = (ThinkNode *)think_outline_content_get_data (content);

	tree = xmlNewChild (node, NULL, "Node", NULL);

	xmlSetProp (tree, "title", tnode->title);

	if (tnode->text && (strlen (tnode->text) != 0))
		xmlSetProp (tree, "text", tnode->text);

	if (!think_outline_content_expanded (content)) {
		xmlSetProp (tree, "expanded", "no");
	}

/* remove the preliminary todo support - it doesn't need to be here. */
#if 0
	if (tnode->is_todo){
		xmlSetProp (tree, "todo", "yes");

		if (tnode->is_done)
			xmlSetProp (tree, "done", "yes");
		else
			xmlSetProp (tree, "done", "no");
	}
	else
		xmlSetProp (tree, "todo", "no");
#endif

	if (g_node_first_child (gnode))
		save_node_xml (g_node_first_child (gnode), tree);

	if (g_node_next_sibling (gnode))
		save_node_xml (g_node_next_sibling (gnode), node);
}

void
outline_save_xml (gchar *filename, GNode *root)
{
	xmlDocPtr doc;

	doc = xmlNewDoc ("1.0");
	doc->root = xmlNewDocNode (doc, NULL, "Outline", NULL);
  
	if (g_node_first_child (root))
		save_node_xml (g_node_first_child (root), doc->root);

	xmlSaveFile (filename, doc);
	xmlFreeDoc (doc);
}
