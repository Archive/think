/* think.h
   Copyright (C) 1999, 2000 Peter Teichman

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, 
   USA.  */

#ifndef __THINK_H__
#define __THINK_H__

#include <gtk/gtk.h>
#include "think-outline.h"

GtkWidget *think_window_new(gchar *filename);

typedef struct _ThinkNode ThinkNode;

struct _ThinkNode
{
	gchar *title;
	gchar *text;
	gboolean is_todo;
	gboolean is_done;
};

GNode *new_think_gnode (void);
ThinkNode *think_node_new (void);
GnomeCanvasItem *think_render_outline_content (GnomeCanvasGroup *group, 
					       ThinkOutlineContent *content);

#endif /* __THINK_H__ defined */
