/* think-outline.c - Think's outline widget
   Copyright (C) 1999, 2000 Peter Teichman

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.  */

#ifndef __THINK_OUTLINE_H__
#define __THINK_OUTLINE_H__

#include <libgnome/gnome-defs.h>
#include <libgnomeui/gnome-canvas.h>

BEGIN_GNOME_DECLS

#define THINK_OUTLINE(obj)            (GTK_CHECK_CAST ((obj), think_outline_get_type (), ThinkOutline))
#define THINK_OUTLINE_CLASS(klass)    (GTK_CHECK_CLASS_CASE ((klass), THINK_TYPE_OUTLINE, ThinkOutlineClass))

typedef struct _ThinkOutline              ThinkOutline;
typedef struct _ThinkOutlineClass         ThinkOutlineClass;
typedef struct _ThinkOutlinePrivate       ThinkOutlinePrivate;

#include "think-outline-content.h"

/* class ThinkOutline begins here. This is the actual outline object */
struct _ThinkOutline {
	GnomeCanvas canvas;

	GnomeCanvasItem *dragged;         /* the item being dragged */
	GnomeCanvasItem *last_entered;    /* the item last dragged onto */
	GnomeCanvasItem *drag_indicator;  /* box or line, depending on where we
					     are currently dragging */

	GNode *tree;       /* root of tree this outline displays */

	ThinkOutlinePrivate *priv;
};

struct _ThinkOutlineClass {
	GnomeCanvasClass parent_class;

	void (* outline_redraw) (ThinkOutline *outline);
	void (* select) (ThinkOutline *outline, ThinkOutlineContent *content);
	void (* item_motion) (ThinkOutline *outline, gdouble x, gdouble y);
	void (* item_release) (ThinkOutline *outline, gdouble x, gdouble y);

	void (* select_next)     (ThinkOutline *outline);
	void (* select_previous) (ThinkOutline *outline);
	void (* select_child)    (ThinkOutline *outline);
	void (* select_parent)   (ThinkOutline *outline);
	void (* toggle_expanded) (ThinkOutline *outline);
};

GtkType    think_outline_get_type (void);
GtkWidget *think_outline_new      (void);
GtkWidget *think_outline_new_with_tree (GNode *tree);
void       think_outline_set_tree (ThinkOutline *outline, GNode *tree);

void       think_outline_redraw   (ThinkOutline *outline);

void       think_outline_select          (ThinkOutline *outline,
					  ThinkOutlineContent *content);
void       think_outline_select_toggle   (ThinkOutline *outline,
					  ThinkOutlineContent *content);
void       think_outline_unselect        (ThinkOutline *outline);

ThinkOutlineContent *think_outline_selection (ThinkOutline *outline);

#define think_outline_get_tree(outline) \
       (THINK_OUTLINE (outline)->tree)

/* private functions - for use by OutlineContent */
void think_outline_draw_box (ThinkOutline *outline, 
			     ThinkOutlineContent *content);

extern double THINK_OUTLINE_Y_OFFSET;

END_GNOME_DECLS

#endif /* __THINK_OUTLINE_H__ not defined */
