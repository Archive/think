/* think.c - most of think's functionality, at this point.
   Copyright (C) 1999, 2000 Peter Teichman

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 03111-1307, 
   USA.  */

#include <config.h>

#include "think.h"
#include "outline.h"
#include <gnome.h>
#include <glade/glade.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "think-outline.h"

/* Callbacks for libglade - all of these can also be called with
 * (NULL, NULL) as args. They can't be declared static, since libglade
 * must be able to look up their symbols. */
void think_new            (void);
void think_save           (void);
void think_exit           (void);

/* these next functions take action on the currently selected item */
void think_clear          (void);
void think_insert_child   (void);
void think_append_child   (void);
void think_insert_sibling (void);
void think_append_sibling (void);

/* copy buffer maintenance */
void think_cut            (void);
void think_copy           (void);
void think_paste          (void);

/* general callbacks */
static void think_title_entry_changed (GtkEntry *widget,    gpointer data);
static void think_text_entry_changed  (GtkEditable *widget, gpointer data);

/* for dealing with the GnomeAppBar */
static void appbar_notify (gchar *text);
static gint appbar_notify_clear (gpointer data);

/* toplevel save and load */
static void outline_save (gchar *filename);
static void outline_load (gchar *filename);

/* useful for GNode iterations */
gboolean destroy_item_content (GNode *node);
gboolean destroy_tree_content (GNode *node);
gboolean free_item_content (GNode *node);
gboolean free_tree_content (GNode *node);

/* make a copy of a full outline tree, including the outline content */
GNode *think_tree_deep_copy (const GNode *node);

/* these should be deprecated */
static void outline_filename_set (gchar *filename);
static void insert_child   (GtkWidget *widget, ThinkOutlineContent *content);
static void append_child   (GtkWidget *widget, ThinkOutlineContent *content);
static void insert_sibling (GtkWidget *widget, ThinkOutlineContent *content);
static void append_sibling (GtkWidget *widget, ThinkOutlineContent *content);
static void remove_content (GtkWidget *widget, ThinkOutlineContent *content);

void file_sel_open (GtkFileSelection *filesel);
void file_sel_save (GtkFileSelection *filesel);

static void outline_set_changed (void);
static void outline_set_clean (void);

GtkWidget *think_outline_create (gchar *string1, gchar *string2, 
				 gint int1, gint int2);

static GNode *tree_root = NULL;
static GladeXML *xml = NULL;

static gchar *outline_filename = NULL;
static gint outline_changed = 0;
static gint making_new = 0; /* horrible, horrible hack */

ThinkNode *
think_node_new (void)
{
	ThinkNode *node = g_malloc (sizeof (ThinkNode));
	node->title = NULL;
	node->text = NULL;
	node->is_todo = 0;
	node->is_done = 0;

	return node;
}

gint
outline_item_event (GtkObject *item, GdkEvent *event, gpointer data)
{
	ThinkOutlineContent *content = THINK_OUTLINE_CONTENT (data);
	ThinkOutline *outline = 
		THINK_OUTLINE (glade_xml_get_widget (xml, "outline"));

	if (event->type == GDK_BUTTON_PRESS){
		if (event->button.button == 3){
			think_outline_select (outline, content);

			gtk_menu_popup 
				(GTK_MENU 
				 (glade_xml_get_widget (xml, "item_popup")), 
				 NULL, NULL, NULL, NULL, 3, 
				 event->button.time);

			return TRUE;
		}
	}

	if (event->type == GDK_2BUTTON_PRESS){
		think_outline_select (outline, content);
		return TRUE;
	}

	return FALSE;
}

GnomeCanvasItem *
think_render_outline_content (GnomeCanvasGroup *group, 
			      ThinkOutlineContent *content)
{
	ThinkNode *node = 
		(ThinkNode *)(think_outline_content_get_data (content));
	GnomeCanvasItem *new_group;
	GnomeCanvasItem *item;
	GtkWidget *label;
	GtkStyle *style;

	new_group = gnome_canvas_item_new (group, GNOME_TYPE_CANVAS_GROUP,
					   "x", (double) 0, "y", (double) 0, 
					   NULL);

	/* nasty hack, create a label just so we can get its font */
	label = gtk_label_new ("");
	gtk_widget_ensure_style (label);
	style = gtk_widget_get_style (label);
	gtk_widget_unref (label);

	item = gnome_canvas_item_new 
		(GNOME_CANVAS_GROUP (new_group),
		 GNOME_TYPE_CANVAS_TEXT,
		 "x", (double) 0, "y", (double) 0,
		 "anchor", GTK_ANCHOR_NW,
		 "text", node->title,
		 "font_gdk", style->font,
		 NULL);

	gtk_signal_connect (GTK_OBJECT (item), "event", 
			    (void *)outline_item_event, content);

	return new_group;
}

void
save_existing_cb (gint reply, gpointer data)
{
	if (reply == GNOME_YES){
		making_new = 1;
		think_save ();
	}

	if (reply == GNOME_NO){
		outline_changed = 0;
		think_new ();
	}
}

void 
think_new (void)
{
	ThinkOutline *outline = 
		THINK_OUTLINE (glade_xml_get_widget (xml, "outline"));
	GtkWidget *main_window = glade_xml_get_widget (xml, "main_window");
	making_new = 0;

	if (outline_changed){
		gnome_app_question (GNOME_APP (main_window), 
				    _("Save existing tree?"), 
				    (void *)save_existing_cb, NULL);
		return;
	}

	if (!tree_root)
		tree_root = g_node_new (outline);
	else
		g_node_children_foreach (tree_root, G_TRAVERSE_ALL, 
					 (void *)free_tree_content, NULL);

	outline_filename_set (NULL);

	think_outline_redraw (outline);
}

void
think_save (void)
{
	gchar *tmp;

	if (!outline_changed){
		gnome_appbar_set_status 
			(GNOME_APPBAR (glade_xml_get_widget (xml, "appbar1")),
			 _("No changes to save."));
		return;
	}

	if (outline_filename == NULL) {
		gtk_widget_show (glade_xml_get_widget (xml, "saveselection"));
		return;
	}

	/* outline_filename is clobbered in outline_save, so we pass a
	 * temporary variable */
	tmp = g_strdup (outline_filename);
	outline_save (tmp);
	g_free (tmp);
}

void 
file_sel_open (GtkFileSelection *filesel)
{
	think_new ();
	outline_load (gtk_file_selection_get_filename (filesel));
}

void
outline_filename_set (gchar *filename)
{
	gchar *title = NULL;

	if (filename) {
		g_free (outline_filename);
		outline_filename = g_strdup (filename);
		title = g_strdup_printf (_("Think - %s"), 
					 g_basename (filename));
	} else {
		title = g_strdup (_("Think"));
	}

	gtk_window_set_title 
		(GTK_WINDOW (glade_xml_get_widget (xml, "main_window")),
		 title);
	g_free (title);
}

void
overwrite_cb (gint reply, gpointer data)
{
	if (reply == GNOME_YES)
		outline_save (data);

	g_free (data);
}

gint
appbar_notify_clear (gpointer data)
{
	GnomeAppBar *appbar = 
		GNOME_APPBAR (glade_xml_get_widget (xml, "appbar1"));

	gnome_appbar_pop (appbar);
	return 0;
}

void
appbar_notify (gchar *text)
{
	GnomeAppBar *appbar = 
		GNOME_APPBAR (glade_xml_get_widget (xml, "appbar1"));
	gnome_appbar_push (appbar, text);
	gtk_timeout_add (3000, appbar_notify_clear, NULL);
}

void
file_sel_save (GtkFileSelection *filesel)
{
	GtkWidget *main_window = glade_xml_get_widget (xml, "main_window");
	char *filename = gtk_file_selection_get_filename (filesel);
	struct stat buf;
	
	if (stat (filename, &buf) == 0){
		gnome_app_question (GNOME_APP (main_window), 
				    _("Overwrite existing file?"), 
				    (void *)overwrite_cb, g_strdup (filename));
	} else {
		outline_save (filename);
	}
}

GNode *
new_think_gnode (void)
{
	gchar *title = _("- empty node -");
	GNode *gnode;
	ThinkOutlineContent *content;
	ThinkNode *node;
  
	node = think_node_new ();
	node->title = g_strdup (title);

	gnode = think_outline_content_new_with_data 
		(node, (void *)think_render_outline_content);

	content = THINK_OUTLINE_CONTENT (gnode->data);
	
	gtk_signal_connect (GTK_OBJECT (content), "expand",
			    (void *)outline_set_changed, NULL);
	gtk_signal_connect (GTK_OBJECT (content), "collapse",
			    (void *)outline_set_changed, NULL);

	gtk_signal_connect (GTK_OBJECT (content), "item_drop",
			    (void *)outline_set_changed, NULL);
	gtk_signal_connect (GTK_OBJECT (content), "item_drop_after",
			    (void *)outline_set_changed, NULL);
	gtk_signal_connect (GTK_OBJECT (content), "item_drop_before",
			    (void *)outline_set_changed, NULL);

	return gnode;
}

void
maybe_create_node (void)
{
	ThinkOutline *outline = 
		THINK_OUTLINE (glade_xml_get_widget (xml, "outline"));
	GNode *gnode;

	gnode = new_think_gnode ();

	g_node_insert (tree_root, 0, gnode);
	think_outline_redraw (outline);
}

void
think_insert_sibling (void)
{
	ThinkOutline *outline = 
		THINK_OUTLINE (glade_xml_get_widget (xml, "outline"));
	if (think_outline_selection (outline))
		insert_sibling (NULL, think_outline_selection (outline));
	else
		maybe_create_node ();

	outline_set_changed ();
}

void 
insert_sibling (GtkWidget *widget, ThinkOutlineContent *content)
{
	ThinkOutline *outline = 
		THINK_OUTLINE (glade_xml_get_widget (xml, "outline"));
	GNode *new_one, *parent, *sibling;

	g_return_if_fail (content != NULL);

	new_one = new_think_gnode ();

	sibling = content->gnode;
	parent = sibling->parent;
	g_node_insert_before (parent, sibling, new_one);

	think_outline_redraw (outline);

	outline_set_changed ();
}

void
think_append_sibling (void)
{
	ThinkOutline *outline = 
		THINK_OUTLINE (glade_xml_get_widget (xml, "outline"));
	if (think_outline_selection (outline))
		append_sibling (NULL, think_outline_selection (outline));
	else
		maybe_create_node ();

	outline_set_changed ();
}

void 
append_sibling (GtkWidget *widget, ThinkOutlineContent *content)
{
	ThinkOutline *outline = 
		THINK_OUTLINE (glade_xml_get_widget (xml, "outline"));
	GNode *new_one, *parent, *sibling;

	g_return_if_fail (content != NULL);

	new_one = new_think_gnode ();

	sibling = content->gnode;
	parent = sibling->parent;
	g_node_insert_before (parent, sibling->next, new_one);

	think_outline_redraw (outline);

	outline_set_changed ();
}

void
think_insert_child (void)
{
	ThinkOutline *outline = 
		THINK_OUTLINE (glade_xml_get_widget (xml, "outline"));
	if (think_outline_selection (outline))
		insert_child (NULL, think_outline_selection (outline));
	else
		maybe_create_node ();

	outline_set_changed ();
}

void
insert_child (GtkWidget *widget, ThinkOutlineContent *content)
{
	ThinkOutline *outline = 
		THINK_OUTLINE (glade_xml_get_widget (xml, "outline"));
	GNode *new_one, *parent;

	g_return_if_fail (content != NULL);

	new_one = new_think_gnode ();

	parent = content->gnode;
	g_node_insert (parent, 0, new_one);
	think_outline_redraw (outline);

	outline_set_changed ();
}

void
think_append_child (void)
{
	ThinkOutline *outline = 
		THINK_OUTLINE (glade_xml_get_widget (xml, "outline"));
	if (think_outline_selection (outline))
		append_child (NULL, think_outline_selection (outline));
	else
		maybe_create_node ();

	outline_set_changed ();
}

void
append_child (GtkWidget *widget, ThinkOutlineContent *content)
{
	ThinkOutline *outline = 
		THINK_OUTLINE (glade_xml_get_widget (xml, "outline"));
	GNode *new_one, *parent;

	g_return_if_fail (content != NULL);
  
	new_one = new_think_gnode ();

	parent = content->gnode;
	g_node_append (parent, new_one);
	think_outline_redraw (outline);

	outline_set_changed ();
}

gboolean
destroy_item_content (GNode *node) 
{
	think_outline_content_hide (THINK_OUTLINE_CONTENT (node->data));
	return FALSE;
}

gboolean
destroy_tree_content (GNode *node) 
{
	g_node_traverse (node, G_IN_ORDER, G_TRAVERSE_ALL, -1,
			 (void *)destroy_item_content, NULL);
	return FALSE;
}

void
think_cut (void)
{
	ThinkOutline *outline =
		THINK_OUTLINE (glade_xml_get_widget (xml, "outline"));
	ThinkOutlineContent *selection;
	GNode *node;
	GNode *old_tree;

	selection = think_outline_selection (outline);
	think_outline_unselect (outline);

	if (selection == NULL) {
		appbar_notify ("cannot cut: no selection");
		return;
	}

	node = selection->gnode;

	destroy_tree_content (node);
	g_node_unlink (selection->gnode);

	old_tree = gtk_object_get_data (GTK_OBJECT (outline), "copy_buffer");
	if (old_tree) {
		g_node_children_foreach (old_tree, G_TRAVERSE_ALL,
					 (void *)free_tree_content, NULL);
	}

	gtk_object_set_data (GTK_OBJECT (outline), "copy_buffer", node);

	think_outline_redraw (outline);
	outline_set_changed ();
}

void
think_paste (void)
{
	ThinkOutline *outline =
		THINK_OUTLINE (glade_xml_get_widget (xml, "outline"));
	ThinkOutlineContent *selection;
	GNode *node;

	selection = think_outline_selection (outline);

	if (selection == NULL) {
		appbar_notify ("cannot paste: no selection");
		return;
	}

	node = gtk_object_get_data (GTK_OBJECT (outline), "copy_buffer");
	if (node == NULL) {
		appbar_notify ("cannot paste: no copy buffer");
		return;
	}

	g_node_insert (selection->gnode, 0, think_tree_deep_copy (node));
	think_outline_redraw (outline);
	outline_set_changed ();
}

gboolean
free_item_content (GNode *node) {
	ThinkNode *tnode = (ThinkNode *)
		(think_outline_content_get_data
		 (THINK_OUTLINE_CONTENT (node->data)));

	g_free (tnode->title);
	g_free (tnode->text);

	think_outline_content_destroy (node);
	return FALSE;
}

gboolean
free_tree_content (GNode *node) 
{
	g_node_traverse (node, G_IN_ORDER, G_TRAVERSE_ALL, -1,
			 (void *)free_item_content, NULL);
	return FALSE;
}

GNode *
think_tree_deep_copy (const GNode *node)
{
	ThinkOutlineContent *content = THINK_OUTLINE_CONTENT (node->data);
	ThinkOutlineContent *new_content;
	ThinkNode *tnode;
	ThinkNode *new_tnode;
	GNode *new_node;

	tnode = (ThinkNode *)think_outline_content_get_data (content);
	new_node = new_think_gnode ();

	new_content = THINK_OUTLINE_CONTENT (new_node->data);

	new_tnode = (ThinkNode *)think_outline_content_get_data (new_content);
	g_free (new_tnode->title);
	new_tnode->title = g_strdup (tnode->title);
	new_tnode->text = g_strdup (tnode->text);
	new_tnode->is_todo = tnode->is_todo;
	new_tnode->is_done = tnode->is_done;

	if (node->children) {
		GNode *tmp;
		for (tmp=node->children; tmp; tmp=tmp->next) {
			g_node_append (new_node, think_tree_deep_copy (tmp));
		}
	}
	return new_node;
}

void
think_copy (void)
{
	ThinkOutline *outline =
		THINK_OUTLINE (glade_xml_get_widget (xml, "outline"));
	ThinkOutlineContent *selection;
	GNode *node;
	GNode *old_tree;
	
	selection = think_outline_selection (outline);

	if (selection == NULL) {
		appbar_notify ("cannot copy: no selection");
		return;
	}

	node = think_tree_deep_copy (selection->gnode);
	old_tree = gtk_object_get_data (GTK_OBJECT (outline), "copy_buffer");
	if (old_tree) {
		g_node_children_foreach (old_tree, G_TRAVERSE_ALL,
					 (void *)free_tree_content, NULL);
	}

	gtk_object_set_data (GTK_OBJECT (outline), "copy_buffer", node);

	think_outline_redraw (outline);
}

void
think_clear (void)
{
	ThinkOutline *outline =
		THINK_OUTLINE (glade_xml_get_widget (xml, "outline"));
	ThinkOutlineContent *selection;
	GNode *node;

	selection = think_outline_selection (outline);

	if (selection == NULL) {
		appbar_notify ("cannot clear: no selection");
		return;
	}
	think_outline_unselect (outline);

	node = selection->gnode;
	free_tree_content (node);
	g_node_unlink (selection->gnode);
	think_outline_redraw (outline);
	outline_set_changed ();
}

void
item_select_cb (ThinkOutline *outline, ThinkOutlineContent *content)
{
	ThinkNode *tnode;
	GtkEntry *entry = GTK_ENTRY (glade_xml_get_widget(xml, "title_entry"));
	GtkText *text = GTK_TEXT (glade_xml_get_widget (xml, "text_entry"));

	if (content == NULL){   /* selection has been cleared */
		gtk_signal_disconnect_by_func (GTK_OBJECT (entry),
					       think_title_entry_changed, 
					       NULL);
		gtk_signal_disconnect_by_func (GTK_OBJECT (text),
					       think_text_entry_changed, 
					       NULL);

		gtk_entry_set_text (entry, "");
		gtk_editable_delete_text (GTK_EDITABLE (text), 0, 
					  gtk_text_get_length 
					  (GTK_TEXT (text)));

		gtk_widget_set_sensitive 
			(glade_xml_get_widget (xml, "vbox1"), FALSE);
		return;
	}

	tnode = (ThinkNode *)think_outline_content_get_data (content);
	gtk_entry_set_text (entry, tnode->title);

	gtk_editable_delete_text (GTK_EDITABLE (text), 0, 
				  gtk_text_get_length (GTK_TEXT (text)));
	gtk_widget_set_sensitive  (glade_xml_get_widget (xml, "vbox1"), TRUE);

	if (tnode->text) {
		gtk_text_insert (text, NULL, NULL, NULL, tnode->text,
				 strlen (tnode->text));
	}

	gtk_signal_connect (GTK_OBJECT (entry), "changed", 
			    think_title_entry_changed, NULL);
	gtk_signal_connect (GTK_OBJECT (text), "changed", 
			    think_text_entry_changed, NULL);

}

void outline_load (gchar *filename)
{
	if (filename != NULL) {
		gchar *notice=NULL;
		if (outline_load_xml (filename, &tree_root))
			outline_filename_set (filename);
		notice = g_strdup_printf (_("Outline loaded: %s"), filename);
		appbar_notify (notice);
		g_free (notice);
	}

	outline_set_clean ();
	think_outline_redraw
		(THINK_OUTLINE (glade_xml_get_widget (xml, "outline")));
}

void outline_save (gchar *filename)
{
	outline_save_xml (filename, tree_root);
	appbar_notify (_("Outline saved."));
	outline_filename_set (filename);
	outline_set_clean ();

	if (making_new)
		think_new ();
}

void
outline_set_changed (void)
{
	outline_changed = 1;
}

void
outline_set_clean (void)
{
	outline_changed = 0;
}

void
reply_cb (gint reply, gpointer callback_data)
{
	if (reply == GNOME_YES)
		gtk_main_quit ();
}

void 
think_exit (void)
{
	GtkWidget *main_window = glade_xml_get_widget (xml, "main_window");
	if (outline_changed){
		gnome_app_question 
			(GNOME_APP (main_window), 
			 _("Outline modified. Exit without saving changes?"), 
			 (void *)reply_cb, NULL);
	} else {
		gtk_main_quit ();
	}
}

void 
think_title_entry_changed (GtkEntry *widget, gpointer data)
{
	ThinkNode *tnode;
	gchar *text;
	ThinkOutline *outline = 
		THINK_OUTLINE (glade_xml_get_widget (xml, "outline"));
	ThinkOutlineContent *content = think_outline_selection (outline);

	tnode = (ThinkNode *)think_outline_content_get_data (content);

	text = tnode->title;
	tnode->title = g_strdup (gtk_entry_get_text (widget));
	g_free (text);

	think_outline_content_set_dirty (content);
	think_outline_redraw (outline);
	outline_set_changed ();
}

void 
think_text_entry_changed (GtkEditable *widget, gpointer data)
{
	ThinkNode *tnode;
	gchar *text;
	ThinkOutline *outline = 
		THINK_OUTLINE (glade_xml_get_widget (xml, "outline"));
	ThinkOutlineContent *content = think_outline_selection (outline);

	tnode = (ThinkNode *)think_outline_content_get_data (content);

	text = tnode->text;
	tnode->text = 
		g_strdup (gtk_editable_get_chars 
			  (widget, 0, 
			   gtk_text_get_length (GTK_TEXT (widget))));

	g_free (text);
	outline_set_changed ();
}

GtkWidget *
think_outline_create (gchar *string1, gchar *string2, gint int1, gint int2)
{
	ThinkOutline *outline;
	tree_root = g_node_new (GINT_TO_POINTER (0xdeadbeef));

	gtk_widget_push_visual (gdk_imlib_get_visual());
	gtk_widget_push_colormap (gdk_imlib_get_colormap());
	outline = THINK_OUTLINE (think_outline_new_with_tree (tree_root));
	gtk_widget_pop_colormap ();
	gtk_widget_pop_visual ();

	gtk_signal_connect (GTK_OBJECT (outline), "select", 
			    (void *)item_select_cb, NULL);
	return GTK_WIDGET (outline);
}

GtkWidget *
think_window_new (gchar *filename)
{
	struct stat tmp;
	gchar *gladefile = "think.glade";

	if (stat (gladefile, &tmp))
		gladefile = DATADIR "/think.glade";

	xml = glade_xml_new (gladefile, NULL);

	if (xml == NULL)
		g_error (_("could not open think.glade file in current directory or '%s'\n"), gladefile);

	glade_xml_signal_autoconnect (xml);
	outline_load (filename);

	return glade_xml_get_widget (xml, "main_window");
}
	
