/* outline-test.c - test the basic functionality of a ThinkOutline
   Copyright (C) 1999, 2000 Peter Teichman

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.  */

#include "think-outline.h"

#include <config.h>
#include <gnome.h>

GnomeCanvasItem *
make_text_item (GnomeCanvasGroup *group, ThinkOutlineContent *content)
{
	GnomeCanvasItem *item;

	item = gnome_canvas_item_new 
		(group, GNOME_TYPE_CANVAS_TEXT,
		 "x", (double) 0, "y", (double) 0, 
		 "anchor", GTK_ANCHOR_NW,
		 "text", think_outline_content_get_data (content),
		 "font", "-*-lucida-medium-r-normal-*-10-*-*-*-*-*-iso8859-1",
		 NULL);

	return item;
}

GnomeCanvasItem *
make_image_item (GnomeCanvasGroup *group, ThinkOutlineContent *content)
{
	GnomeCanvasItem *item;
	GdkImlibImage *image = 
		(GdkImlibImage *)think_outline_content_get_data (content);

	item = gnome_canvas_item_new (group,
				      GNOME_TYPE_CANVAS_IMAGE,
				      "x", (double) 0, "y", (double) 0,
				      "width", (double) image->rgb_width,
				      "height", (double) image->rgb_height,
				      "anchor", GTK_ANCHOR_NW,
				      "image", image, NULL);
	return item;
}

GnomeCanvasItem *
make_widget_item (GnomeCanvasGroup *group, ThinkOutlineContent *content)
{
	GnomeCanvasItem *item;
	GtkWidget *widget = GTK_WIDGET (think_outline_content_get_data (content));

	item = gnome_canvas_item_new (group,
				      GNOME_TYPE_CANVAS_WIDGET,
				      "widget", widget,
				      "x", (double) 0, "y", (double) 0,
				      "width", 100.0, "height", 40.0,
				      "anchor", GTK_ANCHOR_NW,
				      "size_pixels", FALSE, NULL);
  
	return item;
}

int 
main(int argc, char *argv[])
{
	GtkWidget *app, *thingy;
	ThinkOutline *outline;
	GdkImlibImage *im;

	GNode *tree, *blah, *blah2, *here;
  
	gchar *message = "test message";

	gnome_init ("testOutline", VERSION, argc, argv);

	app = gnome_app_new ("testOutline", "testOutline");
	gtk_signal_connect(GTK_OBJECT(app), "delete_event",
			   GTK_SIGNAL_FUNC(gtk_main_quit), NULL);

	tree = g_node_new (app);

	blah = think_outline_content_new_with_data (message, (void *)make_text_item);
	g_node_append (tree, blah);

	blah2 = think_outline_content_new_with_data ("blah message", 
						     (void *)make_text_item);
	g_node_append (blah, blah2);

	here = think_outline_content_new_with_data ("blah child",
						    (void *)make_text_item);
	g_node_append (blah2, here);

	here = think_outline_content_new_with_data ("blah child 2",
						    (void *)make_text_item);
	g_node_append (blah2, here);

	im = gdk_imlib_load_image ("think-logo-tiny.png");
	blah2 = think_outline_content_new_with_data (im,
						     (void *)make_image_item);
	g_node_append (blah, blah2);

	blah2 = think_outline_content_new_with_data ("and another",
						     (void *)make_text_item);
	g_node_append (blah, blah2);

	blah = think_outline_content_new_with_data ("foo",
						    (void *)make_text_item);
	g_node_append (tree, blah);
  
	blah2 = think_outline_content_new_with_data ("bar",
						     (void *)make_text_item);
	g_node_append (blah, blah2);


	blah = think_outline_content_new_with_data ("baz",
						    (void *)make_text_item);
	g_node_append (blah2, blah);

	im = gdk_imlib_load_image ("think-logo-tiny.png");
	blah2 = think_outline_content_new_with_data (im,
						     (void *)make_image_item);
	g_node_append (blah, blah2);

	blah = think_outline_content_new_with_data ("quux",
						    (void *)make_text_item);
	g_node_append (blah2, blah);

	gtk_widget_push_visual (gdk_imlib_get_visual());
	gtk_widget_push_colormap (gdk_imlib_get_colormap());
	outline = THINK_OUTLINE (think_outline_new ());
	gtk_widget_pop_colormap ();
	gtk_widget_pop_visual ();

	think_outline_set_tree (outline, tree);

	thingy = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (thingy),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (thingy),
					       GTK_WIDGET (outline));


	gtk_widget_set_usize (thingy, 300, 300);
	gnome_app_set_contents (GNOME_APP(app), thingy);

	think_outline_redraw (outline);

	gtk_widget_show (GTK_WIDGET(outline));
	gtk_widget_show (thingy);

	gtk_widget_show(app);
	gtk_main();

	return 0;
}
