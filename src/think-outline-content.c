/* think-outline-content.c: base content item for a ThinkOutline
   Copyright (C) 1999, 2000 Peter Teichman

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, 
   USA.  */

#include <config.h>
#include <gnome.h>
#include "think-outline-content.h"

#include "tree_minus.xpm"
#include "tree_plus.xpm"

static void think_outline_content_class_init (ThinkOutlineContentClass *class);
static void think_outline_content_init       (ThinkOutlineContent *content);

static void content_expand_real (ThinkOutlineContent *content);
static void content_collapse_real (ThinkOutlineContent *content);
static void expanded_toggle (ThinkOutlineContent *content);

static void drag_enter (ThinkOutlineContent *content,
			ThinkOutlineContent *dragged);
static void drag_leave (ThinkOutlineContent *content,
			ThinkOutlineContent *dragged);
static void item_drop  (ThinkOutlineContent *content,
			ThinkOutlineContent *dropped);
static void item_drop_after  (ThinkOutlineContent *content, 
			      ThinkOutlineContent *dropped);
static void item_drop_before (ThinkOutlineContent *content, 
			      ThinkOutlineContent *dropped);
static void image_events (GdkImlibImage *image, GdkEvent *event, gpointer data);

static GnomeCanvasGroupClass *think_outline_content_parent_class;

/* distance moved before a click is considered a drag */
#define DRAG_OFFSET 5.0

struct _ThinkOutlineContentPrivate
{
	gpointer data;
	ThinkOutlineContentDrawFunc draw_func;

	guint expanded : 1;
	guint dirty : 1;

	GnomeCanvasItem *tree_image;
};

/* signals */
enum {
	EXPAND,
	COLLAPSE,
	EXPANDED_TOGGLE,
	DRAG_ENTER,
	DRAG_LEAVE,
	ITEM_DROP,
	ITEM_DROP_AFTER,
	ITEM_DROP_BEFORE,
	LAST_SIGNAL
};

/* arguments for content */
enum {
	ARG_0,
	ARG_CONTENT_DATA
};

static guint content_signals[LAST_SIGNAL] = { 0 };

static GdkImlibImage *plus_image = NULL;
static GdkImlibImage *minus_image = NULL;

GtkType
think_outline_content_get_type (void)
{
	static GtkType think_outline_content_type = 0;

	if(!think_outline_content_type) {
		GtkTypeInfo think_outline_content_info = {
			"ThinkOutlineContent",
			sizeof (ThinkOutlineContent),
			sizeof (ThinkOutlineContentClass),
			(GtkClassInitFunc) think_outline_content_class_init,
			(GtkObjectInitFunc) think_outline_content_init,
			NULL,
			NULL,
			(GtkClassInitFunc) NULL
		};

		think_outline_content_type = gtk_type_unique (GNOME_TYPE_CANVAS_GROUP,
							      &think_outline_content_info);
	}
  
	return think_outline_content_type;
}

static void
think_outline_content_class_init (ThinkOutlineContentClass *klass)
{
	GtkWidgetClass *widget_class;

	widget_class = (GtkWidgetClass *) klass;

	think_outline_content_parent_class = 
		gtk_type_class (GTK_TYPE_OBJECT);

	content_signals[EXPAND] =
		gtk_signal_new ("expand",
				GTK_RUN_FIRST,
				GTK_OBJECT_CLASS(klass)->type,
				GTK_SIGNAL_OFFSET (ThinkOutlineContentClass, expand),
				gtk_marshal_NONE__NONE,
				GTK_TYPE_NONE, 0);

	content_signals[COLLAPSE] =
		gtk_signal_new ("collapse",
				GTK_RUN_FIRST,
				GTK_OBJECT_CLASS(klass)->type,
				GTK_SIGNAL_OFFSET (ThinkOutlineContentClass, collapse),
				gtk_marshal_NONE__NONE,
				GTK_TYPE_NONE, 0);

	content_signals[EXPANDED_TOGGLE] =
		gtk_signal_new ("expanded_toggle",
				GTK_RUN_FIRST,
				GTK_OBJECT_CLASS(klass)->type,
				GTK_SIGNAL_OFFSET (ThinkOutlineContentClass, 
						   expanded_toggle),
				gtk_marshal_NONE__NONE,
				GTK_TYPE_NONE, 0);

	content_signals[DRAG_ENTER] =
		gtk_signal_new ("drag_enter",
				GTK_RUN_FIRST,
				GTK_OBJECT_CLASS(klass)->type,
				GTK_SIGNAL_OFFSET (ThinkOutlineContentClass, 
						   drag_enter),
				gtk_marshal_NONE__NONE,
				GTK_TYPE_NONE, 0);

	content_signals[DRAG_LEAVE] =
		gtk_signal_new ("drag_leave",
				GTK_RUN_FIRST,
				GTK_OBJECT_CLASS(klass)->type,
				GTK_SIGNAL_OFFSET (ThinkOutlineContentClass, 
						   drag_leave),
				gtk_marshal_NONE__NONE,
				GTK_TYPE_NONE, 0);

	content_signals[ITEM_DROP] =
		gtk_signal_new ("item_drop",
				GTK_RUN_FIRST,
				GTK_OBJECT_CLASS(klass)->type,
				GTK_SIGNAL_OFFSET (ThinkOutlineContentClass, 
						   item_drop),
				gtk_marshal_NONE__POINTER,
				GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

	content_signals[ITEM_DROP_AFTER] =
		gtk_signal_new ("item_drop_after",
				GTK_RUN_FIRST,
				GTK_OBJECT_CLASS(klass)->type,
				GTK_SIGNAL_OFFSET (ThinkOutlineContentClass, 
						   item_drop_after),
				gtk_marshal_NONE__POINTER,
				GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

	content_signals[ITEM_DROP_BEFORE] =
		gtk_signal_new ("item_drop_before",
				GTK_RUN_FIRST,
				GTK_OBJECT_CLASS(klass)->type,
				GTK_SIGNAL_OFFSET (ThinkOutlineContentClass, 
						   item_drop_before),
				gtk_marshal_NONE__POINTER,
				GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

	gtk_object_class_add_signals (GTK_OBJECT_CLASS (klass), content_signals,
				      LAST_SIGNAL);
  
	klass->expand = (void *)content_expand_real;
	klass->collapse = (void *)content_collapse_real;
	klass->expanded_toggle = (void *)expanded_toggle;

	klass->drag_enter = (void *)drag_enter;
	klass->drag_leave = (void *)drag_leave;
	klass->item_drop = (void *)item_drop;
	klass->item_drop_after = (void *)item_drop_after;
	klass->item_drop_before = (void *)item_drop_before;

	if (!plus_image)
		plus_image = gdk_imlib_create_image_from_xpm_data (tree_plus);

	if (!minus_image)
		minus_image = gdk_imlib_create_image_from_xpm_data (tree_minus);
}

static void
think_outline_content_init (ThinkOutlineContent *content)
{
	content->priv = g_new (ThinkOutlineContentPrivate, 1);

	content->priv->data = NULL;
	content->priv->expanded = 1;

	content->priv->tree_image = NULL;
	content->item = NULL;
}

GNode *
think_outline_content_new (void)
{
	GNode *gnode;
	ThinkOutlineContent *content;
  
	content = gtk_type_new (think_outline_content_get_type ());
	gnode = g_node_new (content);

	content->gnode = gnode;

	return gnode;
}

GNode *
think_outline_content_new_with_data (gpointer data,
				     ThinkOutlineContentDrawFunc func)
{
	GNode *gnode;
	ThinkOutlineContent *content;

	gnode = think_outline_content_new ();

	content = THINK_OUTLINE_CONTENT (gnode->data);
  
	think_outline_content_set_data (content, data);
	think_outline_content_set_draw_func (content, func);

	return gnode;
}

gpointer
think_outline_content_get_data (ThinkOutlineContent *content)
{
	g_return_val_if_fail (content != NULL, NULL);

	return content->priv->data;
}

void
think_outline_content_set_data (ThinkOutlineContent *content, gpointer data)
{
	g_return_if_fail (content != NULL);
	g_return_if_fail (data != NULL);

	content->priv->data = data;
}

void
think_outline_content_set_draw_func (ThinkOutlineContent *content,
				     ThinkOutlineContentDrawFunc func)
{
	g_return_if_fail (content != NULL);

	content->priv->draw_func = func;
}

guint
think_outline_content_expanded (ThinkOutlineContent *content)
{
	g_return_val_if_fail (content != NULL, 0);

	return content->priv->expanded;
}

static void
content_expand_real (ThinkOutlineContent *content)
{
	content->priv->expanded = 1;
}

void
think_outline_content_expand (ThinkOutlineContent *content)
{
	g_return_if_fail (content != NULL);

	if (!content->priv->expanded)
		gtk_signal_emit (GTK_OBJECT (content),
				 gtk_signal_lookup ("expand", 
						    GTK_OBJECT_TYPE (content)));
	content_expand_real (content);
}

static void
content_collapse_real (ThinkOutlineContent *content)
{
	content->priv->expanded = 0;
}

void
think_outline_content_collapse (ThinkOutlineContent *content)
{
	g_return_if_fail (content != NULL);

	if (content->priv->expanded)
		gtk_signal_emit (GTK_OBJECT (content),
				 gtk_signal_lookup ("collapse", 
						    GTK_OBJECT_TYPE (content)));
	content_collapse_real (content);
}

static void
expanded_toggle (ThinkOutlineContent *content)
{
	if (content->priv->expanded) {
		if (content->priv->expanded)
			gtk_signal_emit (GTK_OBJECT (content),
					 gtk_signal_lookup ("collapse", 
							    GTK_OBJECT_TYPE (content)));
		content->priv->expanded = 0;
	} else {
		if (!content->priv->expanded)
			gtk_signal_emit (GTK_OBJECT (content),
					 gtk_signal_lookup ("expand", 
							    GTK_OBJECT_TYPE (content)));
		content->priv->expanded = 1;
	}
}

void
image_events (GdkImlibImage *image, GdkEvent *event, gpointer data)
{
	ThinkOutlineContent *content = THINK_OUTLINE_CONTENT (data);
	static ThinkOutlineContent *clicked = NULL;

	switch (event->type){
	case GDK_BUTTON_PRESS:
		clicked = content;
		break;
      
	case GDK_BUTTON_RELEASE:
		if (clicked == content){
			double x1, x2, y1, y2;

			gnome_canvas_item_get_bounds (GNOME_CANVAS_ITEM 
						      (content->priv->tree_image),
						      &x1, &y1, &x2, &y2);

			if ((event->button.x >= x1) && (event->button.x <= x2)
			    && (event->button.y >= y1) && (event->button.y <= y2)){
				expanded_toggle (content);
				think_outline_redraw (THINK_OUTLINE 
						      (GNOME_CANVAS_ITEM 
						       (content->item)->canvas));
			}
		}
		clicked = NULL;
		break;
	}
}

static void
item_events (GnomeCanvasItem *item, GdkEvent *event, gpointer data)
{
	gdouble x1, y1;
	static gdouble x_orig = 0.0, y_orig = 0.0;
	static gdouble x_click = 0.0, y_click = 0.0;
	static gboolean dragging = FALSE;

	switch (event->type){
	case GDK_BUTTON_PRESS:
		if (event->button.button != 1)
			break;

		THINK_OUTLINE (item->canvas)->dragged = item;

		gnome_canvas_item_get_bounds (item, &x_orig, &y_orig, NULL, NULL);

		x_click = event->button.x;
		y_click = event->button.y;

		break;

	case GDK_MOTION_NOTIFY:
		if (!(event->motion.state & GDK_BUTTON1_MASK))
			break;

		if (THINK_OUTLINE (item->canvas)->dragged){
			if ((x_click - event->motion.x > DRAG_OFFSET)
			    || (event->motion.x - x_click > DRAG_OFFSET)
			    || (y_click - event->motion.y > DRAG_OFFSET)
			    || (event->motion.y - y_click > DRAG_OFFSET)){
				dragging = TRUE;
			}

			if (dragging){
				gnome_canvas_item_raise_to_top (item);
	      
				gnome_canvas_item_get_bounds (item,
							      &x1, &y1, NULL, NULL);
	      
				gnome_canvas_item_move 
					(item, event->motion.x - x1, event->motion.y - y1);
	      
				gtk_signal_emit 
					(GTK_OBJECT (item->canvas),
					 gtk_signal_lookup ("item_motion",
							    GTK_OBJECT_TYPE (item->canvas)),
					 event->motion.x, event->motion.y);
			}
		}
		break;

	case GDK_BUTTON_RELEASE:
		if (event->button.button != 1)
			break;

		if (!dragging){
			think_outline_select_toggle 
				(THINK_OUTLINE (item->canvas), 
				 gtk_object_get_data (GTK_OBJECT (item), "outline_content"));
		}
		else{
			gtk_signal_emit
				(GTK_OBJECT (item->canvas),
				 gtk_signal_lookup ("item_release",
						    GTK_OBJECT_TYPE (item->canvas)),
				 event->button.x, event->button.y);
	  
			if (THINK_OUTLINE (item->canvas)->dragged){
				gnome_canvas_item_get_bounds (item,
							      &x1, &y1, NULL, NULL);
	      
				gnome_canvas_item_move (item, x_orig - x1, y_orig - y1);
			}

			x_orig = 0.0;
			y_orig = 0.0;
	  
			THINK_OUTLINE (item->canvas)->dragged = NULL;
			dragging = FALSE;

			think_outline_redraw (THINK_OUTLINE (item->canvas));	  
		}

		break;
	}
}

static void
drag_events (ThinkOutline *outline, gdouble x, gdouble y, gpointer data)
{
	double x1, x2, y1, y2;
  
	gpointer item_content = NULL, dragged_content = NULL, last_content = NULL;

	g_return_if_fail (data != NULL);

	if (!outline->dragged)
		return;

	if (outline->dragged == data)
		return;

	item_content = gtk_object_get_data (GTK_OBJECT (data), "outline_content");
	dragged_content = 
		gtk_object_get_data (GTK_OBJECT (outline->dragged), "outline_content");

	if (outline->last_entered)
		last_content = 
			gtk_object_get_data (GTK_OBJECT (outline->last_entered), 
					     "outline_content");

	if (outline->last_entered){
		gnome_canvas_item_get_bounds (GNOME_CANVAS_ITEM (outline->last_entered),
					      NULL, &y1, NULL, &y2);
		if ((y < y1) || (y > y2)){
			gtk_signal_emit (GTK_OBJECT (last_content),
					 gtk_signal_lookup 
					 ("drag_leave", GTK_OBJECT_TYPE (last_content)), 
					 dragged_content);
			outline->last_entered = NULL;

			if (outline->drag_indicator){
				gtk_object_destroy (GTK_OBJECT (outline->drag_indicator));
				outline->drag_indicator = NULL;
			}  
		}
	}

	gnome_canvas_item_get_bounds (GNOME_CANVAS_ITEM (data),
				      &x1, &y1, &x2, &y2);

	if ((y > y1) && (y < y2)){
		if (outline->last_entered != data){
			outline->last_entered = data;
	  
			gtk_signal_emit (GTK_OBJECT (item_content),
					 gtk_signal_lookup 
					 ("drag_enter", GTK_OBJECT_TYPE (item_content)), 
					 dragged_content);
	  
			if (outline->drag_indicator){
				gtk_object_destroy (GTK_OBJECT (outline->drag_indicator));
				outline->drag_indicator = NULL;
			}
  
			outline->drag_indicator =
				gnome_canvas_item_new (GNOME_CANVAS_GROUP
						       (GNOME_CANVAS_ITEM (data)->canvas->root),
						       gnome_canvas_rect_get_type (),
						       "x1", (double)(x1-1.0),
						       "y1", (double)(y1-1.0),
						       "x2", (double)x2,
						       "y2", (double)y2,
						       "outline_color", "black",
						       "width_pixels", 1, NULL);
	  
			gnome_canvas_item_lower_to_bottom (outline->drag_indicator);
		}
	}
	else if ((y >= y2) && (y - y2 < (THINK_OUTLINE_Y_OFFSET / 2.0))){
		GnomeCanvasPoints *points;

		if (outline->drag_indicator){
			gtk_object_destroy (GTK_OBJECT (outline->drag_indicator));
			outline->drag_indicator = NULL;
		}

		points = gnome_canvas_points_new (2);
		points->coords[0] = 0;
		points->coords[1] = y2 + THINK_OUTLINE_Y_OFFSET / 4.0;
		points->coords[2] = x2;
		points->coords[3] = y2 + THINK_OUTLINE_Y_OFFSET / 4.0;

		outline->drag_indicator =
			gnome_canvas_item_new (GNOME_CANVAS_GROUP
					       (GNOME_CANVAS_ITEM (data)->canvas->root),
					       gnome_canvas_line_get_type (),
					       "points", points,
					       "fill_color", "black",
					       "width_units", 1.0,
					       NULL);
			       
		gnome_canvas_points_free (points);		       
	}
	else if ((y <= y1) && (y1 - y < (THINK_OUTLINE_Y_OFFSET / 2.0))){
		GnomeCanvasPoints *points;

		if (outline->drag_indicator){
			gtk_object_destroy (GTK_OBJECT (outline->drag_indicator));
			outline->drag_indicator = NULL;
		}
  
		points = gnome_canvas_points_new (2);
		points->coords[0] = 0;
		points->coords[1] = y1 - THINK_OUTLINE_Y_OFFSET / 4.0;
		points->coords[2] = x2;
		points->coords[3] = y1 - THINK_OUTLINE_Y_OFFSET / 4.0;

		outline->drag_indicator =
			gnome_canvas_item_new (GNOME_CANVAS_GROUP
					       (GNOME_CANVAS_ITEM (data)->canvas->root),
					       gnome_canvas_line_get_type (),
					       "points", points,
					       "fill_color", "black",
					       "width_units", 1.0,
					       NULL);
			       
		gnome_canvas_points_free (points);		       
	}

	return;
}

static void
drop_event (ThinkOutline *outline, gdouble x, gdouble y, gpointer data)
{
	double y1, y2;
  
	gpointer item_content = NULL, dragged_content = NULL;

	g_return_if_fail (data != NULL);

	if (!outline->dragged)
		return;

	if (outline->dragged == data)
		return;

	item_content = gtk_object_get_data (GTK_OBJECT (data), "outline_content");
	dragged_content = 
		gtk_object_get_data (GTK_OBJECT (outline->dragged), "outline_content");

	if (outline->drag_indicator){
		gtk_object_destroy (GTK_OBJECT (outline->drag_indicator));
		outline->drag_indicator = NULL;
	}

	gnome_canvas_item_get_bounds (GNOME_CANVAS_ITEM (data),
				      NULL, &y1, NULL, &y2);

	if ((y > y1) && (y < y2)){
		gtk_signal_emit (GTK_OBJECT (item_content),
				 gtk_signal_lookup 
				 ("item_drop", GTK_OBJECT_TYPE (item_content)), 
				 dragged_content);

		outline->last_entered = NULL;
		outline->dragged = NULL;
	}
	else if ((y >= y2) && (y - y2 < (THINK_OUTLINE_Y_OFFSET / 2.0))){
		gtk_signal_emit (GTK_OBJECT (item_content),
				 gtk_signal_lookup 
				 ("item_drop_after", GTK_OBJECT_TYPE (item_content)), 
				 dragged_content);

		outline->last_entered = NULL;
		outline->dragged = NULL;
	}
	else if ((y <= y1) && (y1 - y < (THINK_OUTLINE_Y_OFFSET / 2.0))){
		gtk_signal_emit (GTK_OBJECT (item_content),
				 gtk_signal_lookup 
				 ("item_drop_before", GTK_OBJECT_TYPE (item_content)), 
				 dragged_content);

		outline->last_entered = NULL;
		outline->dragged = NULL;
	}

	return;
}

void
think_outline_content_show (ThinkOutline *outline,
			    ThinkOutlineContent *content,
			    double x, double y, double *width, double *height)
{
	double x1, x2, y1, y2;

	if (content->priv->dirty){ 
		think_outline_content_hide (content);
		content->priv->dirty = 0;
	}

	if (content->priv->tree_image){
		gtk_object_destroy (GTK_OBJECT 
				    (GNOME_CANVAS_ITEM (content->priv->tree_image)));
		content->priv->tree_image = NULL;
	}

	if (g_node_first_child (content->gnode)){
		GdkImlibImage *im;

		if (content->priv->expanded)
			im = minus_image;
		else
			im = plus_image;

		content->priv->tree_image = 
			gnome_canvas_item_new (GNOME_CANVAS_GROUP
					       (GNOME_CANVAS (outline)->root),
					       GNOME_TYPE_CANVAS_IMAGE,
					       "x", (double) 0, "y", (double) 0,
					       "width", (double) im->rgb_width,
					       "height", (double) im->rgb_height,
					       "anchor", GTK_ANCHOR_NW,
					       "image", im, NULL);
      
		gtk_signal_connect (GTK_OBJECT (content->priv->tree_image),
				    "event", image_events, content);

		gnome_canvas_item_get_bounds (content->priv->tree_image,
					      &x1, &y1, NULL, NULL);

		gnome_canvas_item_move (content->priv->tree_image, (double)(x-x1),
					(double)(y-y1));
	}

	x += plus_image->rgb_width + 5;

	if (!content->item){
		content->item = content->priv->draw_func (GNOME_CANVAS_GROUP
							  (GNOME_CANVAS (outline)->root),
							  content);
      
		gtk_object_set_data (GTK_OBJECT (content->item), "outline_content",
				     content);

		gtk_signal_connect (GTK_OBJECT (content->item), "event", 
				    (void *)item_events, content);

		gtk_signal_connect (GTK_OBJECT (outline), "item_motion",
				    (void *)drag_events, content->item);

		gtk_signal_connect (GTK_OBJECT (outline), "item_release",
				    (void *)drop_event, content->item);
	}

	gnome_canvas_item_get_bounds (content->item, &x1, &y1, &x2, &y2);

	gnome_canvas_item_move (content->item, (double)(x-x1), (double)(y-y1));

	if (think_outline_selection (outline) == content)
		think_outline_draw_box (outline, content);

	*width = x + (x2 - x1);
	*height = y2 - y1;
}

void
think_outline_content_hide (ThinkOutlineContent *content)
{
	if (content->priv->tree_image){
		gtk_object_destroy (GTK_OBJECT (content->priv->tree_image));
		content->priv->tree_image = NULL;
	}

	if (content->item){
		gtk_signal_disconnect_by_func (GTK_OBJECT (content->item->canvas), 
					       (void *)drag_events, content->item);
		gtk_signal_disconnect_by_func (GTK_OBJECT (content->item->canvas), 
					       (void *)drop_event, content->item);
		gtk_object_destroy (GTK_OBJECT (content->item));
		content->item = NULL;
	}
}

void
think_outline_content_set_dirty (ThinkOutlineContent *content)
{
	g_return_if_fail (content != NULL);
	content->priv->dirty = 1;
}

static gboolean
hide_subtree (GNode *gnode, gpointer data)
{
	if (THINK_IS_OUTLINE_CONTENT (gnode->data))
		think_outline_content_hide (THINK_OUTLINE_CONTENT (gnode->data));
	return FALSE;
}

void
think_outline_content_destroy (GNode *gnode)
{
	g_return_if_fail (gnode != NULL);

	g_node_traverse (gnode, G_IN_ORDER, G_TRAVERSE_ALL, -1, 
			 (void *)hide_subtree, NULL);

	g_node_destroy (gnode);
}

static void 
drag_enter (ThinkOutlineContent *content, ThinkOutlineContent *dragged)
{
}

static void 
drag_leave (ThinkOutlineContent *content, ThinkOutlineContent *dragged)
{
}

static void 
item_drop (ThinkOutlineContent *content, ThinkOutlineContent *dropped)
{
	if (g_node_is_ancestor (dropped->gnode, content->gnode))
		return;

	g_node_unlink (dropped->gnode);
	g_node_prepend (content->gnode, dropped->gnode);
}

static void 
item_drop_after (ThinkOutlineContent *content, ThinkOutlineContent *dropped)
{
	if (g_node_is_ancestor (dropped->gnode, content->gnode))
		return;
  
	g_node_unlink (dropped->gnode);
	g_node_insert_before (content->gnode->parent, 
			      g_node_next_sibling (content->gnode), dropped->gnode);
}

static void 
item_drop_before (ThinkOutlineContent *content, ThinkOutlineContent *dropped)
{
	if (g_node_is_ancestor (dropped->gnode, content->gnode))
		return;

	g_node_unlink (dropped->gnode);
	g_node_insert_before (content->gnode->parent, 
			      content->gnode, dropped->gnode);
}
