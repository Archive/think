# think sk.po
# Copyright (C) 2002 Free Software Foundation, Inc.
# Marcel Telka <marcel@telka.sk>, 2002.
#
msgid ""
msgstr ""
"Project-Id-Version: think\n"
"POT-Creation-Date: 2002-05-13 13:30+0200\n"
"PO-Revision-Date: 2002-05-15 09:33+0200\n"
"Last-Translator: Marcel Telka <marcel@telka.sk>\n"
"Language-Team: Slovak <sk-i18n@lists.linux.sk>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-2\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/think.c:196
msgid "Save existing tree?"
msgstr "Ulo�i� existuj�ci strom?"

#: src/think.c:220
msgid "No changes to save."
msgstr "�iadne zmeny na ulo�enie."

#: src/think.c:251
#, c-format
msgid "Think - %s"
msgstr "Think - %s"

#: src/think.c:254 src/think.desktop.in.h:2 src/think.glade.h:20
msgid "Think"
msgstr "Think"

#: src/think.c:300
msgid "Overwrite existing file?"
msgstr "Prep�sa� existuj�ci s�bor?"

#: src/think.c:310
msgid "- empty node -"
msgstr "- pr�zdny uzol -"

#: src/think.c:706
#, c-format
msgid "Outline loaded: %s"
msgstr "Skica na��tan�: %s"

#: src/think.c:719
msgid "Outline saved."
msgstr "Skica ulo�en�"

#: src/think.c:753
msgid "Outline modified. Exit without saving changes?"
msgstr "Skica zmenen�. Ukon�i� bez ulo�enia zmien?"

#: src/think.c:830
#, c-format
msgid "could not open think.glade file in current directory or '%s'\n"
msgstr "nem��em otvori� s�bor think.glade v aktu�lnom adres�ri alebo '%s'\n"

#: src/support.c:100 src/support.c:138
#, c-format
msgid "Couldn't find pixmap file: %s"
msgstr "Nem��em n�js� s�bor s obr�zkom: %s"

#: src/support.c:116
#, c-format
msgid "Couldn't create pixmap from file: %s"
msgstr "Nem��em vytvori� obr�zok zo s�boru: %s"

#: src/think.desktop.in.h:1
msgid "Outliner"
msgstr "Skic�r"

#: src/think.glade.h:1
msgid "(C) 2000 Peter Teichman"
msgstr "(C) 2000 Peter Teichman"

#: src/think.glade.h:2
msgid "Append child"
msgstr "Pripoji� potomka"

#: src/think.glade.h:3
msgid "Append sibling"
msgstr "Pripoji� kolegu"

#: src/think.glade.h:4
msgid "Cancel"
msgstr "Zru�i�"

#: src/think.glade.h:5
msgid "Create a new outline"
msgstr "Vytvori� nov� skicu"

#: src/think.glade.h:6
msgid "Insert child"
msgstr "Vlo�i� potomka"

#: src/think.glade.h:7
msgid "Insert sibling"
msgstr "Vlo�i� kolegu"

#: src/think.glade.h:8
msgid "New"
msgstr "Nov�"

#: src/think.glade.h:9
msgid "New File"
msgstr "Nov� s�bor"

#: src/think.glade.h:10
msgid "New child"
msgstr "Nov� potomok"

#: src/think.glade.h:11
msgid "New sibling"
msgstr "Nov� kolega"

#: src/think.glade.h:12
msgid "OK"
msgstr "Ok"

#: src/think.glade.h:13
msgid "Open"
msgstr "Otvori�"

#: src/think.glade.h:14
msgid "Open File"
msgstr "Otvori� s�bor"

#: src/think.glade.h:15
msgid "Open tree"
msgstr "Otvori� strom"

#: src/think.glade.h:16
msgid "Remove"
msgstr "Odstr�ni�"

#: src/think.glade.h:17
msgid "Save"
msgstr "Ulo�i�"

#: src/think.glade.h:18
msgid "Save File"
msgstr "Ulo�i� s�bor"

#: src/think.glade.h:19
msgid "Text"
msgstr "Text"

#: src/think.glade.h:21
msgid "Think is an outline thingy."
msgstr "Think je skic�r."

#: src/think.glade.h:22
msgid "Title"
msgstr "Titulok"

#: src/think.glade.h:23
msgid "_Edit"
msgstr "_Upravi�"

#: src/think.glade.h:24
msgid "_New Outline"
msgstr "_Nov� skica"
